
package com.embedded.pace.sensor;

import java.util.concurrent.ArrayBlockingQueue;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.embedded.pace.WriteLog;
import com.embedded.pace.util.Globals;

public class SensorService extends Service implements SensorEventListener {

    private SensorManager mSensorManager;

    private Sensor mAccelerometer;

    private ArrayBlockingQueue<Reading> mBuffer;

    private WriteLog writer;

    private boolean serviceRunning = false;

    private boolean onStart;

    public static final String TAG = "PACE TEST";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        Log.d(TAG, "Created SensorService");
        mSensorManager = (SensorManager)this.getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mBuffer = new ArrayBlockingQueue<Reading>(Globals.BUFFER_SIZE);
        writer = new WriteLog("PACElog.txt");
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Destroyed SensorService");
        writer.write("> STOPPED SERVICE");
        mSensorManager.unregisterListener(this, mAccelerometer);
        serviceRunning = false;
        stopSelf();

    }

    @Override
    public void onStart(Intent intent, int startid) {
        Log.d(TAG, "started SensorService");
        serviceRunning = true;
        writer.write("> STARTED SERVICE");
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        // FASTEST � 50 sensor updates per second
        onStart = true;

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        double[] acc = {
                0.0, 0.0, 0.0
        };
        double gravity = -9.82;

        // Convert to iOS format to match training data
        // Raw accelerometer data / -9.82
        acc[0] = event.values[0] / gravity;
        acc[1] = event.values[1] / gravity;
        acc[2] = event.values[2] / gravity;

        Reading r = new Reading(acc[0], acc[1], acc[2]);
        if (!mBuffer.offer(r)) {
            // Buffer is full now
            // discard first readings (wrong values)
            if (onStart) {
                mBuffer.clear();
                onStart = false;
                return;
            }
            Reading[] array = (Reading[])mBuffer.toArray(new Reading[0]);
            new OnSensorChangedTask().execute(array);
            mBuffer.clear();
            mSensorManager.unregisterListener(this);
            waitForCoolDown();
        }
    }

    private void waitForCoolDown() {
        new CountDownTimer(10000, 10000) {
            @Override
            public void onFinish() {
                if (serviceRunning) {
                    mSensorManager.registerListener(SensorService.this, mAccelerometer,
                            SensorManager.SENSOR_DELAY_FASTEST);
                }
            }

            @Override
            public void onTick(long millisUntilFinished) {
                // do nothing
            }
        }.start();
    }

    private class OnSensorChangedTask extends AsyncTask<Reading, Void, Void> {

        @Override
        protected Void doInBackground(Reading... params) {
            double[] X = new double[Globals.BUFFER_SIZE];
            double[] Y = new double[Globals.BUFFER_SIZE];
            double[] Z = new double[Globals.BUFFER_SIZE];
            double[] X_copy = new double[Globals.BUFFER_SIZE];
            double[] Y_copy = new double[Globals.BUFFER_SIZE];
            double[] Z_copy = new double[Globals.BUFFER_SIZE];
            for (int i = 0; i < params.length; i++) {
                Reading r = params[i];
                X[i] = r.getX();
                Y[i] = r.getY();
                Z[i] = r.getZ();
            }
            System.arraycopy(X, 0, X_copy, 0, Globals.BUFFER_SIZE);
            System.arraycopy(Y, 0, Y_copy, 0, Globals.BUFFER_SIZE);
            System.arraycopy(Z, 0, Z_copy, 0, Globals.BUFFER_SIZE);
            double x_mean = Globals.mean(X);
            double y_mean = Globals.mean(Y);
            double z_mean = Globals.mean(Z);
            double x_dev = Globals.deviation(X, x_mean);
            double y_dev = Globals.deviation(Y, y_mean);
            double z_dev = Globals.deviation(Z, z_mean);
            double x_energy = Globals.energy(X_copy);
            double y_energy = Globals.energy(Y_copy);
            double z_energy = Globals.energy(Z_copy);
            double xy_corr = Globals.correlation(X, Y, x_mean, y_mean, x_dev, y_dev);
            double xz_corr = Globals.correlation(X, Z, x_mean, z_mean, x_dev, z_dev);
            double yz_corr = Globals.correlation(Y, Z, y_mean, z_mean, y_dev, z_dev);

            Double[] features = new Double[12];
            features[0] = x_mean;
            features[1] = y_mean;
            features[2] = z_mean;
            features[3] = x_dev;
            features[4] = y_dev;
            features[5] = z_dev;
            features[6] = x_energy;
            features[7] = y_energy;
            features[8] = z_energy;
            features[9] = xy_corr;
            features[10] = xz_corr;
            features[11] = yz_corr;

            try {

                int category = (int)(Classifier.classify(features));

                LocalBroadcastManager bMan = LocalBroadcastManager
                        .getInstance(getApplicationContext());

                if (!Globals.CURRENT_CATEGORY.equalsIgnoreCase(Globals.ALL_CATEGORIES[category])) {
                    writer.write(Globals.ALL_CATEGORIES[category]);
                }
                Globals.CURRENT_CATEGORY = Globals.ALL_CATEGORIES[category];
                Intent intent = new Intent(Globals.INTENT_CLASSIFICATION_RESULT);
                intent.putExtra(Globals.KEY_CLASSIFICATION_RESULT, category);
                bMan.sendBroadcast(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

}
