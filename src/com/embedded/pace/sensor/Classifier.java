
package com.embedded.pace.sensor;

//import android.util.Log;

public class Classifier {

    public static double classify(Object[] i) throws Exception {
        double p = Double.NaN;
        p = Classifier.N309436530(i);
        return p;
    }

    static double N309436530(Object[] i) {
        double p = Double.NaN;
        if (i[4] == null) {
            p = 0;
        } else if (((Double)i[4]).doubleValue() <= 0.188711942165) {
            p = Classifier.N527a9c0f1(i);
        } else if (((Double)i[4]).doubleValue() > 0.188711942165) {
            p = Classifier.Nea224b56(i);
        }
        return p;
    }

    static double N527a9c0f1(Object[] i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 0;
        } else if (((Double)i[5]).doubleValue() <= 0.0858938893675) {
            p = 0;
        } else if (((Double)i[5]).doubleValue() > 0.0858938893675) {
            p = Classifier.N6f9bc7162(i);
        }
        return p;
    }

    static double N6f9bc7162(Object[] i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 0;
        } else if (((Double)i[7]).doubleValue() <= 606.469353063) {
            p = Classifier.N5c2bae983(i);
        } else if (((Double)i[7]).doubleValue() > 606.469353063) {
            p = 0;
        }
        return p;
    }

    static double N5c2bae983(Object[] i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 2;
        } else if (((Double)i[6]).doubleValue() <= 135.601234951) {
            p = Classifier.N37d3ac6e4(i);
        } else if (((Double)i[6]).doubleValue() > 135.601234951) {
            p = 0;
        }
        return p;
    }

    static double N37d3ac6e4(Object[] i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 2;
        } else if (((Double)i[6]).doubleValue() <= 45.998033292) {
            p = 2;
        } else if (((Double)i[6]).doubleValue() > 45.998033292) {
            p = Classifier.N29dd86645(i);
        }
        return p;
    }

    static double N29dd86645(Object[] i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 1;
        } else if (((Double)i[1]).doubleValue() <= -0.929474560614) {
            p = 1;
        } else if (((Double)i[1]).doubleValue() > -0.929474560614) {
            p = 2;
        }
        return p;
    }

    static double Nea224b56(Object[] i) {
        double p = Double.NaN;
        if (i[4] == null) {
            p = 1;
        } else if (((Double)i[4]).doubleValue() <= 0.679464886699) {
            p = Classifier.N3717ee947(i);
        } else if (((Double)i[4]).doubleValue() > 0.679464886699) {
            p = Classifier.N7168c1e120(i);
        }
        return p;
    }

    static double N3717ee947(Object[] i) {
        double p = Double.NaN;
        if (i[11] == null) {
            p = 0;
        } else if (((Double)i[11]).doubleValue() <= -0.439533055658) {
            p = Classifier.N43052ce8(i);
        } else if (((Double)i[11]).doubleValue() > -0.439533055658) {
            p = Classifier.N7904d97611(i);
        }
        return p;
    }

    static double N43052ce8(Object[] i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 0;
        } else if (((Double)i[1]).doubleValue() <= -0.841026238095) {
            p = 0;
        } else if (((Double)i[1]).doubleValue() > -0.841026238095) {
            p = Classifier.N8c54889(i);
        }
        return p;
    }

    static double N8c54889(Object[] i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 0;
        } else if (((Double)i[5]).doubleValue() <= 0.272071934231) {
            p = 0;
        } else if (((Double)i[5]).doubleValue() > 0.272071934231) {
            p = Classifier.N7d01acff10(i);
        }
        return p;
    }

    static double N7d01acff10(Object[] i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 1;
        } else if (((Double)i[5]).doubleValue() <= 0.362073060136) {
            p = 1;
        } else if (((Double)i[5]).doubleValue() > 0.362073060136) {
            p = 2;
        }
        return p;
    }

    static double N7904d97611(Object[] i) {
        double p = Double.NaN;
        if (i[8] == null) {
            p = 1;
        } else if (((Double)i[8]).doubleValue() <= 17.1734493109) {
            p = Classifier.N2d61100c12(i);
        } else if (((Double)i[8]).doubleValue() > 17.1734493109) {
            p = Classifier.N3f649b1a13(i);
        }
        return p;
    }

    static double N2d61100c12(Object[] i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 1;
        } else if (((Double)i[3]).doubleValue() <= 0.313723248528) {
            p = 1;
        } else if (((Double)i[3]).doubleValue() > 0.313723248528) {
            p = 0;
        }
        return p;
    }

    static double N3f649b1a13(Object[] i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 1;
        } else if (((Double)i[0]).doubleValue() <= 0.738822131658) {
            p = Classifier.N6602e87914(i);
        } else if (((Double)i[0]).doubleValue() > 0.738822131658) {
            p = Classifier.N3b2ab74f19(i);
        }
        return p;
    }

    static double N6602e87914(Object[] i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 1;
        } else if (((Double)i[2]).doubleValue() <= -0.36013713357) {
            p = Classifier.N48ec944115(i);
        } else if (((Double)i[2]).doubleValue() > -0.36013713357) {
            p = Classifier.N23f95cce16(i);
        }
        return p;
    }

    static double N48ec944115(Object[] i) {
        double p = Double.NaN;
        if (i[8] == null) {
            p = 1;
        } else if (((Double)i[8]).doubleValue() <= 253.961119718) {
            p = 1;
        } else if (((Double)i[8]).doubleValue() > 253.961119718) {
            p = 2;
        }
        return p;
    }

    static double N23f95cce16(Object[] i) {
        double p = Double.NaN;
        if (i[4] == null) {
            p = 1;
        } else if (((Double)i[4]).doubleValue() <= 0.597661188786) {
            p = 1;
        } else if (((Double)i[4]).doubleValue() > 0.597661188786) {
            p = Classifier.N483bead517(i);
        }
        return p;
    }

    static double N483bead517(Object[] i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 2;
        } else if (((Double)i[5]).doubleValue() <= 0.477500628477) {
            p = 2;
        } else if (((Double)i[5]).doubleValue() > 0.477500628477) {
            p = Classifier.N55b66aff18(i);
        }
        return p;
    }

    static double N55b66aff18(Object[] i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 1;
        } else if (((Double)i[1]).doubleValue() <= -0.964654783333) {
            p = 1;
        } else if (((Double)i[1]).doubleValue() > -0.964654783333) {
            p = 2;
        }
        return p;
    }

    static double N3b2ab74f19(Object[] i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 2;
        } else if (((Double)i[0]).doubleValue() <= 0.94625941291) {
            p = 2;
        } else if (((Double)i[0]).doubleValue() > 0.94625941291) {
            p = 1;
        }
        return p;
    }

    static double N7168c1e120(Object[] i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 2;
        } else if (((Double)i[1]).doubleValue() <= -1.091228735) {
            p = Classifier.N1490d4f221(i);
        } else if (((Double)i[1]).doubleValue() > -1.091228735) {
            p = 2;
        }
        return p;
    }

    static double N1490d4f221(Object[] i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 1;
        } else if (((Double)i[3]).doubleValue() <= 0.607732009935) {
            p = 1;
        } else if (((Double)i[3]).doubleValue() > 0.607732009935) {
            p = 2;
        }
        return p;
    }
}
