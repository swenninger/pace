
package com.embedded.pace;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.embedded.pace.util.DistanceEstimator;
import com.embedded.pace.util.Globals;
import com.embedded.pace.util.ScanComparator;

/**
 * The main module of this project. It combines the recognized movement with the
 * estimated velocity and the readings from previous WiFi scans
 * 
 * @author "Sebastian Wenninger"
 */
public class SensingManager extends BroadcastReceiver {

    /**
     * Constants for the connected and disconnected states
     */
    public static boolean STATE_CONNECTED;

    /**
     * The distance the user moved since the last sensing
     */
    private float distance;

    /**
     * The threshold to trigger a new Wifi scan
     */
    private double distance_threshold;

    /**
     * RSS threshold to trigger a new scan when connected (in dB). Average of
     * top 3 AP's RSS in range
     */
    private int rss_threshold;

    /**
     * Average transmission range of a router (in meters)
     */
    private int transmission_avg;

    /**
     * probability to find a new AP after moving distance_threshold
     */
    private double detection_prob;

    /**
     * The movement category we get from the Classifier
     */
    private int category;

    /**
     * Utility class to compute the distance between two sensings
     */
    private DistanceEstimator mEstimator;

    private WriteLog log;

    private WifiManager mWifi;

    public SensingManager(Context context) {
        mEstimator = new DistanceEstimator();
        mEstimator.setLastReading(System.currentTimeMillis());
        mWifi = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        ConnectivityManager mConnect = (ConnectivityManager)context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = mConnect.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (info.isConnectedOrConnecting())
            STATE_CONNECTED = true;
        else
            STATE_CONNECTED = false;

        // Default values
        rss_threshold = -55;
        transmission_avg = 30;
        detection_prob = 0.3;
        distance_threshold = transmission_avg;
        log = new WriteLog("WifiLog.txt");
    }

    public void startWifi() {
        if (!mWifi.isWifiEnabled())
            mWifi.setWifiEnabled(true);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // New movement classification available
        if (intent.getAction().equals(Globals.INTENT_CLASSIFICATION_RESULT)) {
            this.setCategory(intent.getExtras().getInt(Globals.KEY_CLASSIFICATION_RESULT));
            this.distance += mEstimator.readingDistance(System.currentTimeMillis(),
                    this.getCategory());
            // Disconnected state
            if (!STATE_CONNECTED) {
                if (this.distance > this.distance_threshold) {
                    // Trigger wifi sensing
                    mWifi.setWifiEnabled(true);
                    mWifi.startScan();
                    log.write("Disconnected Scan started");
                    this.distance = 0;
                }
            } else {
                // Connected State
                WifiInfo info = mWifi.getConnectionInfo();
                // compute scanning probability
                double d_unit = 10;
                double beta = 0.1;
                double prob = beta + Math.min(Math.floor(this.distance / d_unit) * beta, 1 - beta);
                // > here since smaller dBm values are better
                if (info.getRssi() > rss_threshold && Math.random() < prob) {
                    mWifi.startScan();
                    log.write("Connected Scan started");
                    this.distance = 0;
                }
            }
        } else if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
            // Results of the last WiFi Scan are available
            List<ScanResult> results = mWifi.getScanResults();
            ScanComparator comp = new ScanComparator();
            Collections.sort(results, comp);

            // The density of available AP's (in AP's per squaremeter)
            // Disk model assumed
            int num_ap = results.size();
            double area = (num_ap * Math.pow(transmission_avg, 2) * Math.PI);
            double density = num_ap / area;

            // update distance_threshold
            double ln = -(Math.log(1 - detection_prob));
            double denom = (2 * transmission_avg * density);
            distance_threshold = 2 * ln / denom;

            List<WifiConfiguration> nets = mWifi.getConfiguredNetworks();
            HashMap<ScanResult, Integer> configured = new HashMap<ScanResult, Integer>();
            for (ScanResult result : results) {
                for (WifiConfiguration conf : nets) {
                    if (Globals.trim(conf.SSID, '\"').equals(result.SSID))
                        configured.put(result, conf.networkId);
                }
            }
            // AP to connect to available
            if (!configured.isEmpty()) {
                // find the WiFi with the best signal strength
                List<ScanResult> keys = new ArrayList<ScanResult>(configured.keySet());
                Collections.sort(keys, comp);

                // update rss_threshold (average of top 3 networks available)
                rss_threshold = 0;
                if (keys.size() >= 3) {
                    for (int i = 0; i < 3; i++)
                        rss_threshold += keys.get(i).level / 3;
                } else {
                    for (ScanResult r : keys)
                        rss_threshold += r.level / keys.size();
                }

                ScanResult bestSignal = keys.get(0);
                if (mWifi.enableNetwork(configured.get(bestSignal), true)) {
                    // Connection established, go to CONNECTED state
                    STATE_CONNECTED = true;
                } else
                    STATE_CONNECTED = false;

            } else {
                // No Wifi to connect to available, turn off wifi, go to
                // DISCONNECTED state
                mWifi.setWifiEnabled(false);
                STATE_CONNECTED = false;
            }

        }

    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public void setLastReading(long ts) {
        this.mEstimator.setLastReading(ts);
    }

    public double getThreshold() {
        return distance_threshold;
    }

    public void setThreshold(double distance_threshold) {
        this.distance_threshold = distance_threshold;
    }
}
