
package com.embedded.pace.util;

import edu.emory.mathcs.jtransforms.fft.DoubleFFT_1D;

/**
 * Utility class for the feature engineering methods Also contains global
 * constants like the buffer size for the Accelerometer data
 * 
 * @author Sebastian Wenninger
 */
public class Globals {

    /**
     * Size of the accelerometer buffer (Default = 150). With DELAY_FASTEST we
     * have a frequency of around 50Hz and with 3s sampling we need 150 entries
     * in the buffer
     */
    public static final int BUFFER_SIZE = 150;

    /**
     * We send the classification result via a local broadcast. This is the key
     * the result is saved in in the broadcast's Intent.Extras.
     */
    public static final String KEY_CLASSIFICATION_RESULT = "category";

    /**
     * ACTION definition required to register Receivers for Broadcasts
     */
    public static final String INTENT_CLASSIFICATION_RESULT = "com.embedded.pace.classification";

    /**
     * Current activity
     */
    public static String CURRENT_CATEGORY = "";

    /**
     * All classified categories as strings
     */
    public static final String[] ALL_CATEGORIES = {
            "not moving", "walking", "jogging", "skipping", "climbing stairs", "descending stairs"
    };

    /**
     * Computes the mean over a series of accelerometer data
     * 
     * @param values - Array of accelerometer readings (of one axis)
     * @return the mean (double precision)
     */
    public static double mean(double[] values) {
        double sum = 0;
        for (double i : values)
            sum += i;
        return sum / values.length;
    }

    /**
     * Computes the standard deviation over a series of accelerometer data
     * 
     * @param values - Array of accelerometer readings (of one axis)
     * @param mean - The mean for the readings
     * @return the deviation (double precision)
     */
    public static double deviation(double[] values, double mean) {
        double n = values.length;
        double sum = 0;
        for (double i : values)
            sum += Math.pow((i - mean), 2);
        return Math.sqrt(sum / n);
    }

    /**
     * Computes the energy over a series of accelerometer data using the FFT
     * (Fast Fourier Transform)
     * 
     * @param values - Array of accelerometer readings (of one axis)
     * @return the energy (double precision)
     */
    public static double energy(double[] values) {
        DoubleFFT_1D fft = new DoubleFFT_1D(values.length);
        fft.realForward(values);
        double sum = 0;
        for (double i : values) {
            sum += Math.pow(Math.abs(i), 2);
        }
        return sum / values.length;
    }

    /**
     * Computes the correlation between the readings along two axes
     * 
     * @param X - The accelerometer readings of one axis
     * @param Y - The accelerometer readings of the other axis
     * @param x_mean - The mean of axis X
     * @param y_mean - The mean of axis Y
     * @param x_dev - The deviation of axis X
     * @param y_dev - The deviation of axis Y
     * @return the correlation (double precision)
     */

    public static double correlation(double[] X, double[] Y, double x_mean, double y_mean,
            double x_dev, double y_dev) {
        double sum = 0;
        int n = X.length;
        for (int i = 0; i < n; i++) {
            sum += ((X[i] - x_mean) * (Y[i] - y_mean));
        }
        double denom = (n - 1) * x_dev * y_dev;
        double retVal = sum / denom;

        return retVal;
    }

    /**
     * Trim characters in prefix and suffix
     * 
     * @param str String
     * @param ch character which has to be removed
     * @return null, if str is null, otherwise string will be returned without
     *         character prefixed/suffixed
     */
    public static String trim(String str, char ch) {
        if (str == null)
            return null;
        int count = str.length();
        int len = str.length();
        int st = 0;
        int off = 0;
        char[] val = str.toCharArray();

        while ((st < len) && (val[off + st] <= ch)) {
            st++;
        }
        while ((st < len) && (val[off + len - 1] <= ch)) {
            len--;
        }
        return ((st > 0) || (len < count)) ? str.substring(st, len) : str;
    }
}
