
package com.embedded.pace.util;

/**
 * This class gets the recognized movement and approximates the distance the
 * user moved since the last reading.
 * 
 * @author "Sebastian Wenninger"
 */
public class DistanceEstimator {

    /**
     * Velocities for the different movements in m/s
     */
    public static final float VELOCITY_STANDING = 0.2f;

    public static final float VELOCITY_WALKING = 2f;

    public static final float VELOCITY_RUNNING = 4f;

    /**
     * The last time a new classification came in
     */
    private long lastReading;

    public DistanceEstimator() {
        this.lastReading = 0;
    }

    public long getLastReading() {
        return lastReading;
    }

    public void setLastReading(long ts) {
        lastReading = ts / 1000;
    }

    public float readingDistance(long ts, int category) {

        float distance = 0;
        if (ts != 0) {
            long seconds = ts / 1000;
            long time = seconds - lastReading;

            switch (category) {
                case 0:
                    distance = time * VELOCITY_STANDING;
                    break;
                case 1:
                    distance = time * VELOCITY_WALKING;
                    break;
                case 2:
                    distance = time * VELOCITY_RUNNING;
                    break;
                default:
                    break;
            }
        }
        setLastReading(ts);
        return distance;
    }
}
