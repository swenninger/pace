
package com.embedded.pace.util;

import java.util.Comparator;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;

public class ScanComparator implements Comparator<ScanResult> {

    @Override
    public int compare(ScanResult lhs, ScanResult rhs) {
        return WifiManager.compareSignalLevel(lhs.level, rhs.level);
    }

}
