
package com.embedded.pace;

import java.util.Timer;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.embedded.pace.util.Globals;

public class MainFragment extends Fragment {

    Timer timer = new Timer();

    long startTime = 0;

    public static Handler handler;

    private volatile Thread updateTimeThread;

    Button stopButton;

    Button startButton;

    TextView activity_duration;

    SensingManager mSensing;

    private WriteLog writer;

    private Intent accelerometerIntent;

    public static final String TAG = "PACE TEST";

    Button.OnClickListener stopClickListener = new Button.OnClickListener() {
        @Override
        public void onClick(View arg0) {
            // Start the background Service
            stopService();

        }
    };

    Button.OnClickListener startClickListener = new Button.OnClickListener() {
        @Override
        public void onClick(View arg0) {
            // Start the background Service
            startService();

            // Set the last reading timestamp when the service is started
            // this way we can compute a distance for the first time
            MainFragment.this.mSensing.setLastReading(System.currentTimeMillis());
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.main_fragment, container, false);
        stopButton = (Button)view.findViewById(R.id.button_stop);
        startButton = (Button)view.findViewById(R.id.button_start);
        activity_duration = (TextView)view.findViewById(R.id.activity_duration);

        startButton.setOnClickListener(startClickListener);
        stopButton.setOnClickListener(stopClickListener);

        handler = new Handler();

        writer = new WriteLog("PACElog.txt");
        writer.write(">> STARTED APPLICATION");
        this.mSensing = new SensingManager(this.getActivity().getApplicationContext());

        return view;

    }

    public void changeFragment(Fragment fragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frame, fragment);
        ft.addToBackStack(null);
        ft.commit();

    }

    private void startService() {
        startButton.setVisibility(View.GONE);
        stopButton.setVisibility(View.VISIBLE);
        activity_duration.setVisibility(View.VISIBLE);
        Log.d(TAG, "started service");
        this.mSensing.startWifi();

        startTime = System.currentTimeMillis();
        updateTimer();
        Globals.CURRENT_CATEGORY = "";

        startTime = System.currentTimeMillis();
        updateTimer();

        accelerometerIntent = new Intent(getActivity(),
                com.embedded.pace.sensor.SensorService.class);

        getActivity().startService(accelerometerIntent);
    }

    private void stopService() {
        startButton.setVisibility(View.VISIBLE);
        stopButton.setVisibility(View.GONE);
        activity_duration.setVisibility(View.INVISIBLE);
        Log.d(TAG, "stopped service");

        updateTimeThread = null; // Closes the updateTimer() thread

        getActivity().stopService(accelerometerIntent);
    }

    public void updateTimer() {

        Runnable timerUpdate = new Runnable() {
            @Override
            public void run() {
                Thread thisThread = Thread.currentThread();
                while (updateTimeThread == thisThread) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            long millis = System.currentTimeMillis() - startTime;
                            int seconds = (int)(millis / 1000);
                            int minutes = seconds / 60;
                            int hours = minutes / 60;
                            seconds = seconds % 60;
                            minutes = minutes % 60;
                            activity_duration.setText(String.format(Globals.CURRENT_CATEGORY
                                    + "\n\n%02d:%02d:%02d", hours, minutes, seconds));
                        }
                    });
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        };
        updateTimeThread = new Thread(timerUpdate);
        updateTimeThread.start();
    }
}
