
package com.embedded.pace;

import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;

import com.embedded.pace.util.Globals;

public class MainActivity extends FragmentActivity {

    LocalBroadcastManager lbm;

    SensingManager man;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        man = new SensingManager(getApplicationContext());

        lbm = LocalBroadcastManager.getInstance(getApplicationContext());
        // register broadcastreceiver
        lbm.registerReceiver(man, new IntentFilter(Globals.INTENT_CLASSIFICATION_RESULT));

        registerReceiver(man, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.frame);

        if (fragment == null) {
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.frame, new MainFragment());
            ft.commit();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // outState.putInt("curChoice", mCurCheckPosition);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public void onDestroy() {
        lbm.unregisterReceiver(man);
        unregisterReceiver(man);
        super.onDestroy();
       
    }
}
