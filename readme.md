Welcome to the PACE Repository
-------------------------------
PACE is a power saving application for Android, that uses the accelerometer to change the Wifi discovery frequency based on the user's movement.
This allows for up to 80% longer battery uptime! 